import string
import time
print 'start parse suncs.xml for us!'
start_time = time.clock()
def main():
    name_table = {'AL':'Alabama',
    'AK':'Alaska',
    'AZ':'Arizona',
    'AR':'Arkansas',
    'CA':'California',
    'CO':'Colorado',
    'CT':'Connecticut',
    'DE':'Delaware',
    'FL':'Florida',
    'GA':'Georgia',
    'HI':'Hawaii',
    'ID':'Idaho',
    'IL':'Illinois',
    'IN':'Indiana',
    'IA':'Iowa',
    'KS':'Kansas',
    'KY':'Kentucky',
    'LA':'Louisiana',
    'ME':'Maine',
    'MD':'Maryland',
    'MA':'Massachusetts',
    'MI':'Michigan',
    'MN':'Minnesota',
    'MS':'Mississippi',
    'MO':'Missouri',
    'MT':'Montana',
    'NE':'Nebraska',
    'NV':'Nevada',
    'NH':'New Hampshire',
    'NJ':'New Jersey',
    'NM':'New Mexico',
    'NY':'New York',
    'NC':'North Carolina',
    'ND':'North Dakota',
    'OH':'Ohio',
    'OK':'Oklahoma',
    'OR':'Oregon',
    'PA':'Pennsylvania',
    'RL':'Rhode Island',
    'SC':'South Carolina',
    'SD':'South Dakota',
    'TN':'Tennessee',
    'TX':'Texas',
    'UT':'Utah',
    'VT':'Vermont',
    'VA':'Virginia',
    'WA':'Washington',
    'WV':'West Virginia',
    'WI':'Wisconsin',
    'WY':'Wyoming'
    }
    allzip = open('./allzips.sav')
    allziplines = allzip.readlines()
    allzip.close()

    currentncs = open('../data/sunncs.xml')
    currentncslines = currentncs.readlines()
    currentncs.close()

    target_lines =  len(currentncslines) - 5
    ##construct dict
    target_dict = {}
    target_start_line = 3

    extra_dict = {}
    extra_ncs_sav = open("./ncsnew.sav")
    extra_ncs_read_lines = extra_ncs_sav.readlines()
    extra_ncs_sav.close()
    extran_ncs_lines = len(extra_ncs_read_lines)

    for loop in xrange(0, extran_ncs_lines, 1):
        pos = extra_ncs_read_lines[loop].rfind('|')
        extra_dict[extra_ncs_read_lines[loop][0:pos]] = extra_ncs_read_lines[loop][pos + 1:]

    for loop in xrange(target_start_line, target_lines, 28):
        target_dict[currentncslines[loop][0:len(currentncslines[loop]) - 1].strip("\r\n")] = loop
        
    ##print target_dict['<location>PR0573</location>']  

    for i in allziplines:
        i = i.strip('\r\n')
        n1 = i.rfind('|')
        n2 = i[n1 + 1:]
        n3 = i.find('|')
        traditional_file_name = i[:n3]
        original_name = i[n3 + 1:n1]
        if(target_dict.has_key('<citycode>%s</citycode>'%n2) == True):
            line_location = target_dict['<citycode>%s</citycode>'%n2]

        else:
            key = original_name[-2:]
            if(name_table.has_key(key)):
                pos = original_name.find(',')
                extra_dict_key = original_name[:pos] +'|' + name_table[key]
                if (extra_dict.has_key(extra_dict_key)):
                    n2 = extra_dict[extra_dict_key].strip("\r\n")

                    if(target_dict.has_key('<citycode>%s</citycode>'%n2) == True):
                        line_location = target_dict['<citycode>%s</citycode>'%n2]
                    else:
                        continue
                else:
                    continue
            else:
                continue
        outfile = open('../us/sun/%s.xml'%traditional_file_name,'w')      
        # for k in xrange(0,28):
        #     outfile.write(currentncslines[line_location -1 + k])
        outfile.writelines(currentncslines[line_location -1:line_location + 27])
        outfile.close()


if __name__ == "__main__":
    main()
    
elapsed_time = time.clock() - start_time

print  "parse the sunncs.xml file in %f seconds"%elapsed_time