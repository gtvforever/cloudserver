import string
import time
import profile
def main():
    print 'start parse sunncs.xml for canada...'

    start_time = time.clock()

    allzip = open('./allpostal.sav')
    currentncs = open('../data/sunncs.xml')
    allziplines= allzip.readlines()
    allzip.close()
    currentncslines= currentncs.readlines()
    currentncs.close()
    target_lines = len(currentncslines) - 5
    ##construct dict
    target_dict = {}
    target_start_line = 3


    for loop in xrange(target_start_line, target_lines, 28):
        target_dict[currentncslines[loop][0:len(currentncslines[loop]) - 1].strip('\r\n')] = loop
        
    loop =0
    for i in allziplines:
        i = i.strip('\r\n')
        n1 = i.rfind('|')
        n2 = i[n1 + 1:]
      
        n3 = i.find('|')
        traditional_file_name = i[0:3] +i[4:n3]
        if(target_dict.has_key('<citycode>%s</citycode>'%n2) == True):
            line_location = target_dict['<citycode>%s</citycode>'%n2]

            outfile = open('../ca/sun/%s.xml'%traditional_file_name,'w')      
            # for k in xrange(0,28):
            #     outfile.write(currentncslines[line_location - 1 + k])
            outfile.writelines(currentncslines[line_location-1: line_location + 27])
            outfile.close()
        else:
            loop += 1

    elapsed_time = time.clock() - start_time

    print  "parse the sunncs.xml for canada in %f seconds"%elapsed_time
    print 'useless item number is %d'%loop
    
if __name__=="__main__":
    main()