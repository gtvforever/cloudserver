#! /usr/bin/python
import string
import time

print 'start parse currentncs.xml for canada...'
start_time = time.clock()
def main():
    tc_name = (
    " ",     #0
    "Sunny",
    "Mostly sunny",
    "Partly sunny",
    "Intermittent clouds",
    "Hazy Sunshine",
    "Mostly cloudy",
    "Cloudy",
    "Dreary",
     " " ,
     " " ,       #10
    "Fog",
    "Showers",
    "Mostly cloudy with showers",
    "Partly sunny with showers",
    "Thunderstorms",
    "Mostly cloudy with thunder showers",
    "Partly sunny with thunder showers",
    "Rain",
    "Flurries",
    "Mostly cloudy with flurries",     #20
    "Partly sunny with flurries",
    "Snow",
    "Mostly cloudy with snow",
    "Ice",
    "Sleet",
    "Freezing rain",
    " " ,
    " " ,
    "Rain and snow mixed",
    "Hot",    #30
    "Cold",
    "Windy",
    "Clear",
    "Mostly Clear",
    "Partly cloudy",
    "Intermittent clouds" ,
    "Hazy",
    "Mostly cloudy",
    "Partly cloudy with showers",    
    "Mostly cloudy with showers", #40
    "Partly cloudy with thunder showers",
    "Mostly cloudy with thunder showers",
    "Mostly cloudy with flurries",
    "Mostly cloudy with Snow")
        
    allzip = open('./allpostal.sav')
    currentncs = open('../data/currentncs.xml')
    allziplines= allzip.readlines()
    currentncslines= currentncs.readlines()

    target_lines = len(currentncslines) - 5
    ##construct dict
    target_dict = {}
    target_start_line = 5


    for loop in range(target_start_line, target_lines, 15):
        target_dict[currentncslines[loop][0:len(currentncslines[loop]) - 1].strip('\r\n')] = loop
        
    ##print target_dict['<location>PR0573</location>']  

    for i in allziplines:
        i = i.strip('\r\n')
        n1 = i.rfind('|')
        n2 = i[n1 + 1:]
      
        n3 = i.find('|')
        traditional_file_name = i[0:3] +i[4:n3]
        original_name = i[n3 + 1:n1]

    ##    print original_name
        if(target_dict.has_key('<location>' + n2 + '</location>') == True):
            line_location = target_dict['<location>' + n2 + '</location>']

            outfile = open('../ca/cur/'+ traditional_file_name + '.xml','w')      
            for k in range(0,15):
                if k==1:
                    outfile.write('<location>' + original_name + '</location>')
                    outfile.write('<origincode>' + traditional_file_name + '</origincode>')
                elif k == 11:
                    index = string.atoi(currentncslines[line_location -1 + k -1][6:8])
                    outfile.write('<phrase>' + tc_name[index] +'</phrase>' + '\n')
                elif k <> 12 and k<>13:
                    outfile.write(currentncslines[line_location -1 + k])  
            outfile.close()

if __name__ == "__main__":
    main()
elapsed_time = time.clock() - start_time

print 'parse file in %f seconds'%elapsed_time
