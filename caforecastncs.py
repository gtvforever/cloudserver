#! /usr/bin/python
import string
import time

print 'start parse forecastncs.xml for canada...'
start_time = time.clock()

def main():
    tc_name = (
    " ",     #0
    "Sunny",
    "Mostly sunny",
    "Partly sunny",
    "Intermittent clouds",
    "Hazy Sunshine",
    "Mostly cloudy",
    "Cloudy",
    "Dreary",
     " " ,
     " " ,       #10
    "Fog",
    "Showers",
    "Mostly cloudy with showers",
    "Partly sunny with showers",
    "Thunderstorms",
    "Mostly cloudy with thunder showers",
    "Partly sunny with thunder showers",
    "Rain",
    "Flurries",
    "Mostly cloudy with flurries",     #20
    "Partly sunny with flurries",
    "Snow",
    "Mostly cloudy with snow",
    "Ice",
    "Sleet",
    "Freezing rain",
    " " ,
    " " ,
    "Rain and snow mixed",
    "Hot",    #30
    "Cold",
    "Windy",
    "Clear",
    "Mostly Clear",
    "Partly cloudy",
    "Intermittent clouds" ,
    "Hazy",
    "Mostly cloudy",
    "Partly cloudy with showers",    
    "Mostly cloudy with showers", #40
    "Partly cloudy with thunder showers",
    "Mostly cloudy with thunder showers",
    "Mostly cloudy with flurries",
    "Mostly cloudy with Snow")
    
    allzip = open('./allpostal.sav')
    currentncs = open('../data/forecastncs.xml')
    allziplines= allzip.readlines()
    currentncslines= currentncs.readlines()

    target_lines = len(currentncslines) - 5
    ##construct dict
    target_dict = {}
    target_start_line = 4


    for loop in range(target_start_line, target_lines, 48):
        target_dict[currentncslines[loop][0:len(currentncslines[loop]) - 1].strip('\r\n')] = loop
        
    ##print target_dict['<location>PR0573</location>']  

    for i in allziplines:
        i = i.strip('\r\n')
        n1 = i.rfind('|')
        n2 = i[n1 + 1:]
        n3 = i.find('|')
        traditional_file_name = i[0:3] +i[4:n3]

        if(target_dict.has_key('<citycode>' + n2 + '</citycode>') == True):
            line_location = target_dict['<citycode>' + n2 + '</citycode>']

            outfile = open('../ca/for/'+ traditional_file_name + '.xml','w')
            for k in range(0,48):
                if k == 5 or k == 14 or k == 23 or k == 32 or k == 41:
                    index = string.atoi(currentncslines[line_location -1 + k -1][6:8])
                    outfile.write('<phrase>' + tc_name[index] +'</phrase>' +'\n')
                elif k<>6 and k<>7 and k<>15 and k<>16 and k<>24 and k<>25 and k<>33 and k<>34 and k<>42 and k<>43:
                    outfile.write(currentncslines[line_location -1 + k]) 
            outfile.close()
    ##    else:
    ##        print n2


if __name__ == "__main__":
    main()
    
elapsed_time = time.clock() - start_time

print  "parse the canada forecast.xml file in %f seconds"%elapsed_time

