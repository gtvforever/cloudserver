import shutil
import time
import os
import zipfile
def zip_dir(dirname,zipfilename):
    filelist = []
    if os.path.isfile(dirname):
        filelist.append(dirname)
    else :
        for root, dirs, files in os.walk(dirname):
            for name in files:
                filelist.append(os.path.join(root, name))

    zf = zipfile.ZipFile(zipfilename, "w", zipfile.zlib.DEFLATED)
    for tar in filelist:
        arcname = tar[len(dirname):]
        #print arcname
        zf.write(tar,arcname)
    zf.close()
    
cur_path = os.getcwd()
src_path = cur_path + '/../data'
dst_path = cur_path + '/../backup/'

exist_folder = os.listdir(dst_path)
print exist_folder
print len(exist_folder)
while (len(exist_folder)>= 72) :
    shutil.rmtree(dst_path + exist_folder[0])
    exist_folder = os.listdir(dst_path)
    
current_time_stamp = time.strftime('%Y_%m_%d_%H_%M_%S',time.localtime(time.time()))
folder_name = dst_path + current_time_stamp+".zip"
print folder_name

#shutil.copytree(src_path,folder_name);
zip_dir(src_path, folder_name)