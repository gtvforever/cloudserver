CREATE TABLE `tux_status_tbl` (
  `mac_addr` varchar(17) NOT NULL,
  `current_version` varchar(255) DEFAULT NULL,
  `previous_version` varchar(255) DEFAULT NULL,
  `ext_ip` varchar(15) DEFAULT NULL,
  `int_ip` varchar(15) DEFAULT NULL,
  `sdcard_status` varchar(64) DEFAULT NULL,
  `sdcard_value` int DEFAULT 0,
  `life_cycle` varchar(8) DEFAULT NULL,
  `user_email`  varchar(255) DEFAULT NULL,
  `user_serial_number` varchar(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mac_addr`),
  UNIQUE KEY `mac_addr_UNIQUE` (`mac_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

REPLACE INTO `device_status_tbl` (`mac_addr`, `current_version`, 
                `previous_version`, `ext_ip`, `int_ip`, `sdcard_status`, 
                `sdcard_value`, `life_cycle`, `user_email`, `user_serial_number`)
VALUES
  ('00:D0:2D:12:34:79', 'TUXW_V5.1.1.5_VA', 'TUXW_V5.1.1.3_VA', '108.170.102.187',
     '10.0.0.8', 'OK', 190, 'A', 'alexconca@gmail.com', '123456875');
