#! /usr/bin/python
import string
import time
import profile
print 'start parse currentncs.xml for us'
start_time = time.clock()
def main():
    tc_name = (
    " ",     #0
    "Sunny",
    "Mostly sunny",
    "Partly sunny",
    "Intermittent clouds",
    "Hazy Sunshine",
    "Mostly cloudy",
    "Cloudy",
    "Dreary",
     " " ,
     " " ,       #10
    "Fog",
    "Showers",
    "Mostly cloudy with showers",
    "Partly sunny with showers",
    "Thunderstorms",
    "Mostly cloudy with thunder showers",
    "Partly sunny with thunder showers",
    "Rain",
    "Flurries",
    "Mostly cloudy with flurries",     #20
    "Partly sunny with flurries",
    "Snow",
    "Mostly cloudy with snow",
    "Ice",
    "Sleet",
    "Freezing rain",
    " " ,
    " " ,
    "Rain and snow mixed",
    "Hot",    #30
    "Cold",
    "Windy",
    "Clear",
    "Mostly Clear",
    "Partly cloudy",
    "Intermittent clouds" ,
    "Hazy",
    "Mostly cloudy",
    "Partly cloudy with showers",    
    "Mostly cloudy with showers", #40
    "Partly cloudy with thunder showers",
    "Mostly cloudy with thunder showers",
    "Mostly cloudy with flurries",
    "Mostly cloudy with Snow")
        
    name_table = {'AL':'Alabama',
    'AK':'Alaska',
    'AZ':'Arizona',
    'AR':'Arkansas',
    'CA':'California',
    'CO':'Colorado',
    'CT':'Connecticut',
    'DE':'Delaware',
    'FL':'Florida',
    'GA':'Georgia',
    'HI':'Hawaii',
    'ID':'Idaho',
    'IL':'Illinois',
    'IN':'Indiana',
    'IA':'Iowa',
    'KS':'Kansas',
    'KY':'Kentucky',
    'LA':'Louisiana',
    'ME':'Maine',
    'MD':'Maryland',
    'MA':'Massachusetts',
    'MI':'Michigan',
    'MN':'Minnesota',
    'MS':'Mississippi',
    'MO':'Missouri',
    'MT':'Montana',
    'NE':'Nebraska',
    'NV':'Nevada',
    'NH':'New Hampshire',
    'NJ':'New Jersey',
    'NM':'New Mexico',
    'NY':'New York',
    'NC':'North Carolina',
    'ND':'North Dakota',
    'OH':'Ohio',
    'OK':'Oklahoma',
    'OR':'Oregon',
    'PA':'Pennsylvania',
    'RL':'Rhode Island',
    'SC':'South Carolina',
    'SD':'South Dakota',
    'TN':'Tennessee',
    'TX':'Texas',
    'UT':'Utah',
    'VT':'Vermont',
    'VA':'Virginia',
    'WA':'Washington',
    'WV':'West Virginia',
    'WI':'Wisconsin',
    'WY':'Wyoming'
    }

    file_allzips_sav = open('./allzips.sav')
    all_zip_lines= file_allzips_sav.readlines()
    file_allzips_sav.close()

    file_currentncs = open('../data/currentncs.xml')
    currentncslines= file_currentncs.readlines()
    file_currentncs.close()
    target_lines = len(currentncslines) - 5
    ##construct dict
    target_dict = {}
    target_start_line = 5

    extra_dict = {}
    extra_ncs_sav = open("./ncsnew.sav")
    extra_ncs_read_lines = extra_ncs_sav.readlines()
    extra_ncs_sav.close()

    extran_ncs_lines = len(extra_ncs_read_lines)
    for loop in xrange(0, extran_ncs_lines, 1):
        pos = extra_ncs_read_lines[loop].rfind('|')
        extra_dict[extra_ncs_read_lines[loop][0:pos]] = extra_ncs_read_lines[loop][pos + 1:]

    # print extra_dict['Abarr|Colorado']
    for loop in xrange(target_start_line, target_lines, 15):
        target_dict[currentncslines[loop][0:len(currentncslines[loop])-1].strip("\r\n")] = loop
        
    # for i in target_dict:
    	# print 
    total=0
    unsupport = 0
    unsuppotkey = 0
    supportkey = 0
    unloca = 0
    entersearch = 0;
    for i in all_zip_lines:
        i = i.strip("\r\n")
        n1 = i.rfind('|')
        n2 = i[n1 + 1:]
        n3 = i.find('|')
        traditional_file_name = i[:n3]
        original_name = i[n3 + 1:n1]
      
        if(target_dict.has_key('<location>%s</location>'%n2) == True):
            line_location = target_dict['<location>%s</location>'%n2]
        else:
            key = original_name[-2:]
            if(name_table.has_key(key)):
                pos = original_name.find(',')
                extra_dict_key = original_name[:pos] +'|' + name_table[key]
                if (extra_dict.has_key(extra_dict_key)):
                    supportkey += 1
                    # print extra_dict[extra_dict_key]
                    # print len(extra_dict[extra_dict_key].strip('\r\n'))
                    # print len(extra_dict[extra_dict_key].strip('\n'))
                    n2 = extra_dict[extra_dict_key].strip("\r\n")

                    if(target_dict.has_key('<location>%s</location>'%n2) == True):
                        line_location = target_dict['<location>%s</location>'%n2]
                        entersearch += 1;
                    else:
                        unloca += 1
                        continue
                else:
                    continue
            else:
                continue

        ##update specific file            
        outfile = open('../us/cur/%s.xml'%traditional_file_name, 'w')      
        for k in xrange(0,15):
            if k == 1:
                outfile.write('<location>%s</location>\n'%original_name)
                outfile.write('<origincode>%s</origincode>\n'%traditional_file_name	)
            elif k == 11:
                index = string.atoi(currentncslines[line_location -1 + k -1][6:8])
                outfile.write('<phrase>%s</phrase>\n'%tc_name[index])
            elif k <> 12 and k <> 13:
                outfile.write(currentncslines[line_location - 1 + k])  
        outfile.close()


if __name__ == "__main__":
    # profile.run('main()')
    main()
elapsed_time = time.clock() - start_time

print 'parse file in %f seconds'%elapsed_time
