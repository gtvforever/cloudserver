import os
import time

print 'start parsing south america....'
starttime = time.clock()

def main():
    currentncs = open('../data/currentncs.xml')
    currentncslines = currentncs.readlines()
    currentncs.close()

    forecastncs = open('../data/forecastncs.xml')
    forecastncslines = forecastncs.readlines()
    forecastncs.close()

    sunncs = open('../data/sunncs.xml')
    sunncslines= sunncs.readlines()
    sunncs.close()
    
    country_name =('ar','chi','uy','pry',
                   'col','bra','mex','per',
                   'dom','cri','pan')

    fold_type = ('cur', 'for', 'sun')

    origin_city_name = (("Buenos Aires", "Cordoba", "Rosario", "Mendoza", "La Plata", "San Miguel de Tucuman", "Mar del Plata", "Santa Fe", "Salta", "San Juan"),
                        ("Santiago", "Valdivia", "Concepcion", "Temuco", "Valparaiso", "Vina del Mar", "Talca", "Puerto Montt", "Punta Arenas", "Osorno"),                          
                        ("Montevideo", "Punta del Este", "Salto", "Colonia", "Paysandu"),
                        ("Asuncion", "Ciudad del Este", "Encarnacion", "Villarrica", "Villeta", "Pedro Juan Caballero", "Concepcion", "Fuerte Olimpo", "Capitan Pablo Lagerenza"),
                        ("Bogota", "Cali", "Medellin", "Barranquilla", "Cartagena", "Manizales", "Neiva", "Bucaramanga", "Villavicencio", "Santa Marta", "Popayan"),
                        ("Rio De Janeiro", "Sao Paulo", "Belo Horizonte", "Fortaleza", "Salvador", "Brasilia", "Manaus", "Recife", "Santos", "Porto Alegre"),
                        ("Mexico City", "Guadalajara", "Monterrey", "Puebla", "Tijuana", "Acapulco De Juarez", "Ciudad Juarez", "Veracruz"),
                        ("Lima", "Cusco", "Arequipa", "Puno", "Trujillo", "Ayacucho", "Huancayo", "Iquitos", "Piura", "Chiclayo"),
                        ("Santo Domingo", "San Felipe De Puerto Plata", "La Romana", "Santa Barbara De Samana", "Santa Cruz De Barahona", "Sosua", "San Pedro De Macoris"),
                        ("San Jose", "Alajuela", "Cartago", "Heredia", "Puntarenas", "Liberia", "Limon"),
                        ("Panama City", "Veraguas", "Cocle","Santiago", "Las Tablas", "Aguadulce", "Colon", "La Chorrera"))

    city_list = (('BUEX','SACO','SAAR','SAME','SALP','SANT','SAZM','SAAV','SASA','SANU'),
                 ('SCSC','SCVV','SCIE','SCTC','CH0113','SCVM','SCTA','SCPM','SCPA','SCOS'),
                 ('SUMU','SUPD','SUSO','SCOX','SUPU'),
                 ('SGAS','SGCE','SGEN','SGSV','PYITA1','SBPP','SGCO','PY0069','PYCAP'),
                 ('MCBO' ,'SKCL','SKMD' ,'SKBQ' ,'SKCG' ,'COMA' ,'SKNV' ,'SKBU' ,'SKVV' ,'SKSM' ,'SKPP'),
                 ('SBRJ' ,'SBSP' ,'SBBH' ,'SBFZ' ,'SBSV' ,'SBBR' ,'SBEG' ,'SBRC' ,'BZSY' ,'SBCO'),
                 ('MMMX' ,'MME8' ,'MTY' ,'MPUB' ,'TIJ' ,'ACAX' ,'CIUY','MMX9'),
                 ('SPIM' ,'SLCU' ,'SPQU' ,'SPPU' ,'SPRU' ,'SPHO' ,'SPHC' ,'SPQT' ,'SPUR' ,'SPHI'),
                 ('MDHE' ,'MDPP' ,'MDLR' ,'MDSN' ,'MDBA' ,'MDPP', 'MDCE'),
                 ('MROC' ,'MROC' ,'CS30' ,'CSHE' ,'MRPU' ,'CSLI','CS63'),
                 ('MPMG' ,'PM0029' ,'PMPEN' ,'MPSA' ,'MPNU' ,'PM0039' ,'MPCO' ,'PMCAP'))

    ##step 0 create subfolder

    for i in xrange(0, len(country_name)):
        if os.path.exists('../' + country_name[i]):
            print country_name[i] + ' is existed'
        else:
            os.mkdir('../' + country_name[i])

        for j in xrange(0, len(fold_type)):
            outname = '../' + country_name[i] + '/' + fold_type[j]
            if os.path.exists(outname):
                print outname + ' is existed'
            else:
                os.mkdir(outname)

            for k in xrange(0, len(city_list[i])):
    ##            print city_list[i][k]
                filename = outname + '/' + city_list[i][k] + '.xml'
    ##            print  filename
                if j==0:
                    search_name = '<location>' + city_list[i][k] + '</location>' + '\r\n'
                    position = currentncslines.index(search_name)
                    fd = open(filename,'w')
                    for loop in range(0, 15):
                        if loop == 1:
                            fd.write('<location>' + origin_city_name[i][k] + '</location>')
                            fd.write('<origincode>' + city_list[i][k] + '</origincode>')
                        else:
                            fd.write(currentncslines[position - 1 + loop])
                    fd.close()
                    
                if j==1:
                    search_name = '<citycode>' + city_list[i][k] + '</citycode>' + '\r\n'
                    position = forecastncslines.index(search_name)
                    fd = open(filename,'w')
                    for loop in range(0, 48):
                        fd.write(forecastncslines[position - 1 + loop])
                    fd.close()
                    
                if j==2:
                    search_name = '<citycode>' + city_list[i][k] + '</citycode>' + '\r\n'
                    position = sunncslines.index(search_name)
                    fd = open(filename,'w')
                    for loop in range(0, 28):
                        fd.write(sunncslines[position - 1 + loop])
                    fd.close()

if __name__ == "__main__":
    main()
print 'parse south america xml in %f seconds'%(time.clock()-starttime) 
